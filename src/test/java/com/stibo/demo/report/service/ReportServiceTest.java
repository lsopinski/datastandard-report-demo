package com.stibo.demo.report.service;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.stibo.demo.report.model.Datastandard;
import lombok.Value;
import org.assertj.core.util.Lists;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import static com.stibo.demo.report.model.ReportHeaders.ATTRIBUTE_NAME;
import static com.stibo.demo.report.model.ReportHeaders.CATEGORY_NAME;
import static com.stibo.demo.report.model.ReportHeaders.DESCRIPTION;
import static com.stibo.demo.report.model.ReportHeaders.GROUPS;
import static com.stibo.demo.report.model.ReportHeaders.TYPE;
import static java.util.stream.Collectors.toList;
import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = {ReportService.class, ObjectMapper.class})
public class ReportServiceTest {

    private static Fixtures fixtures = new Fixtures();
    private Datastandard datastandard;

    @Autowired
    ObjectMapper objectMapper;

    @Autowired
    ReportService reportService;


    @Before
    public void before() throws IOException {
        InputStream stream = getClass().getClassLoader().getResourceAsStream("datastandard.json");
        this.datastandard = objectMapper.readValue(stream, Datastandard.class);
    }

    @Test
    public void testReportToLeafLevel() {
        // when
        List<List<String>> report = reportService.report(datastandard, "leaf").map(row -> row.collect(toList())).collect(toList());

        // then
        assertThat(report).isEqualTo(fixtures.rowsOfLeafReport);
    }

    @Test
    public void testReportToChildLevel() {
        // when
        List<List<String>> report = reportService.report(datastandard, "child").map(row -> row.collect(toList())).collect(toList());

        // then
        assertThat(report).isEqualTo(fixtures.rowsOfChildReport);
    }

    @Value
    private static class Fixtures {
        String categoryNameHeader = CATEGORY_NAME.toString();
        String attributeNameHeader = ATTRIBUTE_NAME.toString();
        String descriptionNameHeader = DESCRIPTION.toString();
        String typeHeader = TYPE.toString();
        String groupsHeader = GROUPS.toString();

        List<String> cells1 = Lists.newArrayList(categoryNameHeader, attributeNameHeader, descriptionNameHeader, typeHeader, groupsHeader);
        List<String> cells2 = Lists.newArrayList("Root",
                "String Value*",
                null,
                "string",
                "All");
        List<String> cells3 = Lists.newArrayList("Leaf",
                "Composite Value",
                "Composite Value Description",
                "composite{\n  Nested Value*: integer\n}[]",
                "All\nComplex");
        List<String> cells4 = Lists.newArrayList("Child",
                "Additional Test Value",
                "Additional test value with multivalue type and optional attribute link",
                "strangeValue{\n  String Value*: string\n}[]",
                "Complex");
        List<List<String>> rowsOfLeafReport = Lists.newArrayList(cells1, cells2, cells3);
        List<List<String>> rowsOfChildReport = Lists.newArrayList(cells1, cells2, cells3, cells4);
    }
}
