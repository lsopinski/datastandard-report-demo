package com.stibo.demo.report.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.function.Function;
import java.util.function.UnaryOperator;
import java.util.stream.Stream;

import com.stibo.demo.report.model.Attribute;
import com.stibo.demo.report.model.AttributeGroup;
import com.stibo.demo.report.model.AttributeLink;
import com.stibo.demo.report.model.AttributeType;
import com.stibo.demo.report.model.Category;
import com.stibo.demo.report.model.Datastandard;
import com.stibo.demo.report.model.Row;
import org.springframework.stereotype.Service;

import static com.stibo.demo.report.model.ReportHeaders.ATTRIBUTE_NAME;
import static com.stibo.demo.report.model.ReportHeaders.CATEGORY_NAME;
import static com.stibo.demo.report.model.ReportHeaders.DESCRIPTION;
import static com.stibo.demo.report.model.ReportHeaders.GROUPS;
import static com.stibo.demo.report.model.ReportHeaders.TYPE;
import static java.lang.String.format;
import static java.util.Collections.reverse;
import static java.util.Optional.ofNullable;
import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.toList;

@Service
public class ReportService {

    public Stream<Stream<String>> report(final Datastandard datastandard, final String categoryId) {
        final List<Category> parents = getParents(datastandard, categoryId);
        final List<Row> rows = new ArrayList<>();
        rows.add(initReportHeaders());

        for (Category parent : parents) {
            rows.addAll(createRowsForCategory(parent, datastandard));
        }

        return rows.stream()
                .map(row -> Stream.of(row.getCategoryName(), row.getAttributeName(), row.getDescription(), row.getType(), row.getGroups()))
                .collect(toList())
                .stream();
    }

    private List<Category> getParents(final Datastandard datastandard, final String categoryId) {
        final Function<String, Category> byId =
                id -> datastandard.getCategories().stream()
                        .filter(category -> category.getId().equals(id))
                        .findFirst()
                        .orElse(null);

        final Category child = byId.apply(categoryId);

        final UnaryOperator<Category> nextParent =
                category -> category == null ? null : byId.apply(category.getParentId());

        final List<Category> parents =
                Stream.iterate(child, nextParent)
                        .limit(datastandard.getCategories().size())
                        .filter(Objects::nonNull)
                        .collect(toList());

        reverse(parents);

        return parents;
    }

    private List<Row> createRowsForCategory(final Category category, final Datastandard datastandard) {
        return getAttributesForCategory(category, datastandard).stream()
                .map(attribute -> Row.builder()
                        .categoryName(category.getName())
                        .attributeName(attribute.getName())
                        .description(attribute.getDescription())
                        .type(getAttributeTypes(attribute, datastandard))
                        .groups(getAttributeGroups(attribute.getGroupIds(), datastandard))
                        .build())
                .collect(toList());
    }

    private List<Attribute> getAttributesForCategory(final Category category, final Datastandard datastandard) {
        final List<String> attributeIds = category.getAttributeLinks().stream()
                .map(AttributeLink::getId)
                .collect(toList());

        final List<String> nonOptionalAttributeIds = getNonOptionalAttributeIds(category);

        datastandard.getAttributes()
                .forEach(attribute -> {
                    if (nonOptionalAttributeIds.contains(attribute.getId())) {
                        attribute.setName(attribute.getName() + "*");
                    }
                });

        return datastandard.getAttributes().stream()
                .filter(attribute -> attributeIds.contains(attribute.getId()))
                .collect(toList());
    }

    private String getAttributeGroups(final List<String> groupIds, final Datastandard datastandard) {
        return ofNullable(datastandard.getAttributeGroups())
                .map(Collection::stream)
                .orElseGet(Stream::empty)
                .filter(attributeGroup -> groupIds.contains(attributeGroup.getId()))
                .map(AttributeGroup::getName)
                .collect(joining("\n"));
    }

    private String getAttributeTypes(final Attribute attribute, final Datastandard datastandard) {
        final List<Attribute> attributesList = datastandard.getAttributes();
        final Attribute attr = attributesList.stream()
                .filter(attribute::equals)
                .findFirst()
                .orElse(null);
        final List<String> attributeLinksList = ofNullable(attribute.getAttributeLinks())
                .map(Collection::stream)
                .orElseGet(Stream::empty)
                .map(AttributeLink::getId)
                .collect(toList());

        if (attr != null && attr.getType().getMultiValue() && attributeLinksList.size() > 0) {
            final List<String> nonOptionalAttributeNames = getNonOptionalAttributeNames(attributesList, attr);

            final String linkedAttributes = attributesList.stream()
                    .map(Attribute::getId)
                    .filter(attributeLinksList::contains)
                    .map(attrId -> attributesList.stream()
                            .filter(attr1 -> attrId.equals(attr1.getId()))
                            .map(Attribute::getName)
                            .map(attrLinkedName -> {
                                if (nonOptionalAttributeNames.contains(attrLinkedName)) {
                                    return format("%s", "  " + attrLinkedName + "*: " + getAttributeTypeByName(attributesList, attrLinkedName));
                                } else {
                                    return format("%s", "  " + attrLinkedName + ": " + getAttributeTypeByName(attributesList, attrLinkedName));
                                }
                            })
                            .findFirst()
                            .orElse(null)
                    )
                    .collect(joining("\n", "\n", "\n"));
            attr.getType().setId(format("%s{%s}[]", attribute.getType().getId(), linkedAttributes));
        } else if (attr != null && attr.getType().getMultiValue()) {
            attr.getType().setId(attribute.getType().getId() + "[]");
        }

        return attr != null ? attr.getType().getId() : null;
    }

    private String getAttributeTypeByName(final List<Attribute> attributeList, final String attributeLinkedName) {
        return attributeList.stream()
                .filter(attribute -> attributeLinkedName.equals(attribute.getName()))
                .map(Attribute::getType)
                .map(AttributeType::getId)
                .findFirst()
                .orElse(null);
    }

    private List<String> getNonOptionalAttributeIds(final Category category) {
        return category.getAttributeLinks().stream()
                .filter(attributeLink -> attributeLink.getOptional().equals(false))
                .map(AttributeLink::getId)
                .collect(toList());
    }

    private List<String> getNonOptionalAttributeNames(final List<Attribute> attributesList, final Attribute attribute) {
        final List<String> nonOptionalAttributeIds = attribute.getAttributeLinks().stream()
                .filter(attrLink -> attrLink.getOptional().equals(false))
                .map(AttributeLink::getId)
                .collect(toList());

        return attributesList.stream()
                .filter(x -> nonOptionalAttributeIds.contains(x.getId()))
                .map(Attribute::getName)
                .collect(toList());
    }

    private Row initReportHeaders() {
        return Row.builder()
                .categoryName(CATEGORY_NAME.toString())
                .attributeName(ATTRIBUTE_NAME.toString())
                .description(DESCRIPTION.toString())
                .type(TYPE.toString())
                .groups(GROUPS.toString())
                .build();
    }
}
