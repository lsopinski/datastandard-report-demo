package com.stibo.demo.report.model;

import java.util.List;

import lombok.Data;

@Data
public class Attribute {
    private String id;
    private String name;
    private String description;
    private AttributeType type;
    private List<AttributeLink> attributeLinks;
    private List<String> groupIds;
}

