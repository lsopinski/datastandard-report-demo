package com.stibo.demo.report.model;

import java.util.List;

import lombok.Data;

@Data
public class Category {
    private String id;
    private String name;
    private String description;
    private String parentId;
    private List<AttributeLink> attributeLinks;
}

