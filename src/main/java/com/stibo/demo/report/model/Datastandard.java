package com.stibo.demo.report.model;

import java.util.List;

import lombok.Data;

@Data
public class Datastandard {
  private String id;
  private String name;
  private List<Category> categories;
  private List<Attribute> attributes;
  private List<AttributeGroup> attributeGroups;
}

