package com.stibo.demo.report.model;

import lombok.Data;

@Data
public class AttributeLink {
    private String id;
    private Boolean optional;
}

