package com.stibo.demo.report.model;

public enum ReportHeaders {
    CATEGORY_NAME("Category Name"),
    ATTRIBUTE_NAME("Attribute Name"),
    DESCRIPTION("Description"),
    TYPE("Type"),
    GROUPS("Groups");

    private final String label;

    ReportHeaders(String label) {
        this.label = label;
    }

    @Override
    public String toString() {
        return this.label;
    }
}
