package com.stibo.demo.report.model;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class Row {
    private String categoryName;
    private String attributeName;
    private String description;
    private String type;
    private String groups;
}
