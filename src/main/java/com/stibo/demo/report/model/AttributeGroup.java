package com.stibo.demo.report.model;

import lombok.Data;

@Data
public class AttributeGroup {
    private String id;
    private String name;
    private String description;
}

