package com.stibo.demo.report.model;

import lombok.Data;

@Data
public class AttributeType {
    private String id;
    private Boolean multiValue;
}

